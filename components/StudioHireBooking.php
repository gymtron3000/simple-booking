<?php namespace Monologophobia\StudioHire\Components;

use DB;
use Mail;
use Response;
use Redirect;
use DateTime;
use Throwable;
use Exception;
use Validator;

use Monologophobia\StudioHire\Models\Studio;
use Monologophobia\StudioHire\Models\Booking;

class StudioHireBooking extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name' => 'Studio Hire Booking',
            'description' => 'Shows available booking slots to customer and lets them book.'
        ];
    }

    public function onRun() {
        $this->page['studios'] = Studio::get();
    }

    public function onGetSlots() {
        $date   = new DateTime(post('date'));
        $studio = Studio::findOrFail(intval(post('studio')));
        $this->page['date']   = $date;
        $this->page['user']   = \Auth::getUser();
        $this->page['studio'] = $studio;
        $this->page['slots']  = $studio->getBookableSlotsForDay($date);
        $this->page['stripe_key'] = $studio->venue->stripe['live'] ? $studio->venue->stripe['publishable'] : $studio->venue->stripe['sandbox_publishable'];
        return ['#slots' => $this->renderPartial('@slots')];
    }

    public function onBook() {
        try {

            $date   = new DateTime(post('date'));
            $studio = Studio::findOrFail(intval(post('studio_id')));

            $times = $this->getTimes($date, post('time'));
            $name  = filter_var(post('name'), FILTER_SANITIZE_STRING);
            $email = filter_var(post('email'), FILTER_SANITIZE_EMAIL);
            if (!$name || !$email) throw new Exception("Data Missing");

            $phone  = filter_var(post('phone'), FILTER_SANITIZE_STRING);
            $reason = filter_var(post('reason'), FILTER_SANITIZE_STRING);

            $slot = $studio->findSlot($times->from, $times->to);

            // Begin a database transaction so we can make use of the model's save validation rather than repeating things
            DB::beginTransaction();

            $booking = $this->generateBooking($name, $email, $times, $phone, $reason, $studio);
            // save the model immediately. we can rollback if payment fails
            $booking->save();

            $secret_key = $studio->venue->stripe['live'] ? $studio->venue->stripe['secret'] : $studio->venue->stripe['sandbox_secret'];

            $return = $this->paymentIntent($email, round($slot->price * 100), $studio->name, $secret_key, post('payment_method_id'), post('payment_intent_id'));
            if ($return->requires_action || $return->error) {
                DB::rollback();
                return Response::json($return);
            }
            else {
                DB::commit();
            }

            // add the payment reference
            Booking::where('id', $booking->id)->update(['payment_reference' => $return->payment_id]);

            try {
                $vars = ['studio' => $studio, 'times' => $times];
                Mail::sendTo($email, 'monologophobia.studiohire::studiohire.booked', $vars);
            }
            catch (Throwable $e) {}

            return Redirect::refresh()->with('message', 'Studio Booked');

        }
        catch (Throwable $e) {
            DB::rollback();
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Gets From and To times from posted time
     * @param DateTime date
     * @param String time as H:i-H:i (from-to)
     * @return Object {'from': DateTime, 'to': DateTime}
     */
    private function getTimes(DateTime $date, string $time) : object {

        $time = explode("-", $time);

        $from = date('H:i:s', strtotime($time[0]));
        $from = explode(":", $from);
        $from_date = clone $date;
        $from_date->setTime($from[0], $from[1], $from[2]);

        $to = date('H:i:s', strtotime($time[1]));
        $to = explode(":", $to);
        $to_date = clone $date;
        $to_date->setTime($to[0], $to[1], $to[2]);

        return (object) ["from" => $from_date, "to" => $to_date];

    }

    /**
     * Generate a booking model and validate it at the same time
     * @param String Name
     * @param String Email
     * @param Object times {'from': DateTime, 'to': DateTime}
     * @param ?String Phone
     * @param ?String Reason
     * @param Studio
     * @return Booking
     */
    private function generateBooking(string $name, string $email, object $times, string $phone = null, string $reason = null, Studio $studio) {

        $booking = new Booking;
        $rules   = $booking->rules;

        $data = [
            "name"   => $name,
            "email"  => $email,
            "from"   => $times->from,
            "to"     => $times->to,
            "phone"  => $phone,
            "reason" => $reason,
            "studio" => $studio,
        ];

        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        foreach ($data as $key => $value) $booking->$key = $value;

        return $booking;

    }

    /**
     * Create or confirm a Payment Intent
     * @param String email for receipt
     * @param Numeric amount (if float, will be automatically converted to pence)
     * @param String description for receipt
     * @param String Stripe Secret Key
     * @param String optional payment_method_id
     * @param String optional payment_intent_id
     * @return Object {requires_action: boolean, payment_intent_client_secret: String}
     * @throws Exception
     */
    private function paymentIntent($email, $amount, $description, $secret_key, $method = false, $intent = false) {

        if (!$method && !$intent) throw new Exception('Method or Intent must be specified');

        \Stripe\Stripe::setApiKey($secret_key);

        $payment_intent = false;
        $return = ['requires_action' => false, 'payment_intent_client_secret' => false, 'error' => false];

        try {

            // Do intent retrieval first as that is more important on the second round trip
            if ($intent) {
                $payment_intent = \Stripe\PaymentIntent::retrieve($intent);
                $payment_intent->confirm();
            }
            else if ($method) {
                $payment_intent = \Stripe\PaymentIntent::create([
                    'amount'      => $amount,
                    'currency'    => 'GBP',
                    'confirm'     => true,
                    'description' => $description,
                    'receipt_email'  => $email,
                    'payment_method' => $method,
                    'confirmation_method' => 'manual',
                ]);
            }

            if ($payment_intent->status == 'requires_action' && $payment_intent->next_action->type == 'use_stripe_sdk') {
                $return['requires_action'] = true;
                $return['payment_intent_client_secret'] = $payment_intent->client_secret;
            }
            else if ($payment_intent->status == 'succeeded') {
                $return['payment_id'] = $payment_intent->id;
            }

        }
        catch (\Stripe\Error\Base $e) {
            $return['error']   = true;
            $return['message'] = $e->getMessage();
        }

        return (object) $return;

    }

}
