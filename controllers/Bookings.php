<?php namespace Monologophobia\StudioHire\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;

use Monologophobia\StudioHire\Models\Studio;

class Bookings extends Controller {

    public $implement = ['Backend\Behaviors\ListController', 'Backend\Behaviors\FormController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.StudioHire', 'main-menu-item', 'side-menu-item2');
    }

    public function formExtendQuery($query) {
        $user = BackendAuth::getUser();
        if (!$user->is_superuser) {
            $studios = Studio::where('venue_id', $user->venue_id)->lists('id');
            $query->whereIn('studio_id', $studios);
        }
    }

    public function listExtendQuery($query) {
        $user = BackendAuth::getUser();
        if (!$user->is_superuser) {
            $studios = Studio::where('venue_id', $user->venue_id)->lists('id');
            $query->whereIn('studio_id', $studios);
        }
    }

}
