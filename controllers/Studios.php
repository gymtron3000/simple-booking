<?php namespace Monologophobia\StudioHire\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;

class Studios extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'monologophobia.studiohire' 
    ];

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.StudioHire', 'main-menu-item', 'side-menu-item');
    }

    public function formExtendQuery($query) {
        $user = BackendAuth::getUser();
        if (!$user->is_superuser) {
            $query->where('venue_id', $user->venue_id);
        }
    }

    public function listExtendQuery($query) {
        $user = BackendAuth::getUser();
        if (!$user->is_superuser) {
            $query->where('venue_id', $user->venue_id);
        }
    }

}
