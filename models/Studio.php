<?php namespace Monologophobia\StudioHire\Models;

use Model;
use DateTime;
use Exception;
use ValidationException;

/**
 * Model
 */
class Studio extends Model {

    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mono_studiohire_studios';

    protected $jsonable = ['date_slots'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required|string',
    ];

    public $belongsTo = [
        'venue' => [
            'Monologophobia\Company\Models\Venue', 'key' => 'venue_id',
        ]
    ];

    public $hasMany = [
        'bookings' => [
            'Monologophobia\StudioHire\Models\Booking', 'key' => 'studio_id', 'delete' => true, 'orderBy' => 'from'
        ]
    ];

    public function beforeSave() {

        // assign studio to venue
        $user = \BackendAuth::getUser();
        if (!$user->is_superuser) $this->venue_id = $user->venue_id;

        // fix times and dates to standard formats
        $fixed = [];
        // do ones offs
        $fixed['oneoff'] = $this->fixTimesAndDates($this->date_slots['oneoff'], true);
        // do days of weeks
        for ($i = 1; $i <= 7; $i++) $fixed[$i] = $this->fixTimesAndDates($this->date_slots[$i]);
        // apply
        $this->date_slots = $fixed;
    }

    /**
     * Make sure times and dates are formatted correctly
     * Framework sends full Y-m-d H:i:s but we just want either the time or the date
     * @param Array slots
     * @param bool is one offs
     * @return Array in corrected format
     */
    private function fixTimesAndDates(array $slots, bool $oneoffs = false) {

        // if this is one offs, make sure the date is Y-m-d only
        if ($oneoffs) {
            foreach ($slots as &$slot) {
                $timestamp = strtotime($slot['specific_date']);
                $slot['specific_date'] = date('Y-m-d', $timestamp);
                $slot['times'] = $this->fixTimes($slot['times']);
            }
        }
        else {
            $slots = $this->fixTimes($slots);
        }

        return $slots;

    }

    /**
     * Make sure times are H:i:S
     * @param Array Slot of times
     * @return Array corrected slot of times
     */
    private function fixTimes(array $slot) : array {
        // make sure times are H:i:s only
        $times = [];
        foreach ($slot as $time) {
            $from = strtotime($time['from']);
            $to   = strtotime($time['to']);
            $times[] = [
                'price' => $time['price'],
                'from'  => date('H:i:s', $from),
                'to'    => date('H:i:s', $to)
            ];
        }
        return $times;
    }

    /**
     * Returns the bookable slots for this day for this studio
     * Checks through all recurring and one-offs. If a one-off exists it has precedence
     * Also checks if any bookings exist for those times
     * @param DateTime
     * @return Array of Slot Objects [{price: double, from: 'H:i', to: 'H:i', booked: bool}]
     */
    public function getBookableSlotsForDay(DateTime $date) : array {

        // find one-off
        foreach ($this->date_slots['oneoff'] as $slot) {
            if ($slot['specific_date'] == $date->format('Y-m-d')) {
                return $this->generateSlots($slot['times'], $date);
            }
        }

        // find specific recurring day
        $day = $date->format('N');
        return $this->generateSlots($this->date_slots[$day], $date);

    }

    /**
     * Generate slots based on supplied slot
     * Also checks if the slot is booked or not
     * @param Array Slots
     * @param DateTime Date
     * @return Array Slots modified
     */
    private function generateSlots(array $times, DateTime $date) : array {
        $return = [];
        foreach ($times as $time) {
            $array = [];
            $array['price']  = $time['price'];
            $array['from']   = $time['from'];
            $array['to']     = $time['to'];
            $array['booked'] = $this->bookings()
                                    ->where('from', "{$date->format('Y-m-d')} {$time['from']}")
                                    ->where('to', "{$date->format('Y-m-d')} {$time['to']}")
                                    ->exists();
            $return[] = (object) $array;
        }
        return $return;
    }

    /**
     * Provides an easy method of checking if a required from and to date are already booked
     * Also checks that the specified dates actually exist as slots
     * @param DateTime $from
     * @param DateTime $to
     * @return boolean
     * @throws Exception if slot does not exist
     */
    public function isDateTimeBookable(DateTime $from, DateTime $to) : bool {
        $found_slot = $this->findSlot($from, $to);
        if (!$found_slot) throw new Exception("Booking Slot not found");
        return $this->bookings()->where('from', $from->format('Y-m-d H:i:s'))->where('to', $to->format('Y-m-d H:i:s'))->doesntExist();
    }

    /**
     * Finds a specific slot based on the from and to time
     * @param DateTime $from
     * @param DateTime $to
     * @return Object of Time Slot or null
     */
    public function findSlot(DateTime $from, DateTime $to) : ?object {
        $slots = $this->getBookableSlotsForDay($from);
        foreach ($slots as $time) {
            if ($time->from == $from->format('H:i:s') && $time->to == $to->format('H:i:s')) {
                return $time;
            }
        }
        return null;
    }

}
