<?php namespace Monologophobia\StudioHire\Models;

use Model;
use Exception;

use Monologophobia\StudioHire\Models\Studio;

/**
 * Model
 */
class Booking extends Model {

    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Nullable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mono_studiohire_bookings';

    protected $dates = ['created_at', 'updated_at', 'from', 'to'];
    protected $nullable = ['phone', 'reason'];
    protected $fillable = ['payment_reference'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required|string',
        'email' => 'required|email',
        'from' => 'required|date',
        'to' => 'required|date',
        'phone' => 'string',

    ];

    public $belongsTo = [
        'studio' => [
            'Monologophobia\StudioHire\Models\Studio', 'key' => 'studio_id'
        ]
    ];

    public function getStudioIdOptions() {
        $user = \BackendAuth::getUser();
        if ($user->is_superuser) return Studio::lists('name', 'id');
        return Studio::where('venue_id', $user->venue_id)->lists('name', 'id');
    }

    public function afterValidate() {
        if (!$this->studio->isDateTimeBookable($this->from, $this->to)) {
            throw new Exception ("Not Bookable");
        }
    }

}
