<?php namespace Monologophobia\StudioHire;

use System\Classes\PluginBase;

class Plugin extends PluginBase {

    public function registerComponents() {
        return ["Monologophobia\StudioHire\Components\StudioHireBooking" => "studiohirebooking"];
    }

    public function registerSettings() {
    }

    public function registerMailTemplates() {
        return [
            'monologophobia.studiohire::studiohire.booked' => "Studio Booked",
        ];
    }

}
